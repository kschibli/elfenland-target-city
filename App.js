import * as React from "react";
import { ScrollView } from "react-native";
import { ButtonGroup, Header, Icon, PricingCard } from "react-native-elements";

export default class App extends React.Component {
  cities = [
    "Al'Baran",
    "Beatá",
    "Dag'Amura",
    "Erg'Erén",
    "Feodor",
    "Grangor",
    "Jaccaranda",
    "Jxara",
    "Kiromah",
    "Lapphálya",
    "Mah'Davikia",
    "Parundia",
    "Rivinia",
    "Strykhaven",
    "Throtmanni",
    "Tichih'",
    "Usselen",
    "Virst",
    "Wylhién",
    "Yttar"
  ];

  colors = ["black", "purple", "blue", "green", "red", "yellow"];

  minPlayers = 2;

  defaultTargetCity = {
    name: "",
    hidden: false
  };

  constructor(props) {
    super(props);
    this.state = {
      players: this.getDefaultPlayers(this.minPlayers)
    };
  }

  getDefaultPlayers = number => {
    return this.colors
      .slice(0, number)
      .map(color => ({ color, targetCity: { ...this.defaultTargetCity } }));
  };

  setTargetCityHidden = (playerToUpdate, hidden) => {
    this.setState(prevState => ({
      ...prevState,
      players: prevState.players.map(player =>
        player.color === playerToUpdate.color
          ? {
              ...player,
              targetCity: {
                ...player.targetCity,
                hidden: !player.targetCity.hidden
              }
            }
          : player
      )
    }));
  };

  selectRandomTargetCity = playerToUpdate => {
    const name = this.cities[Math.trunc(this.cities.length * Math.random())];
    const targetCity = { name, hidden: false };
    this.setState(prevState => ({
      ...prevState,
      players: prevState.players.map(player =>
        player.color === playerToUpdate.color
          ? { ...player, targetCity }
          : player
      )
    }));
  };

  handleNumberOfPlayersPress = index => {
    const numberOfPlayers = index + this.minPlayers;
    this.setState(prevState => ({
      ...prevState,
      players: prevState.players
        .concat(this.getDefaultPlayers(numberOfPlayers))
        .filter(
          (thing, index, self) =>
            self.findIndex(t => t.color === thing.color) === index
        )
        .slice(0, numberOfPlayers)
    }));
  };

  handleResetButtonPress = () => {
    this.setState(prevState => ({
      ...prevState,
      players: prevState.players.map(player => ({
        ...player,
        targetCity: { ...this.defaultTargetCity }
      }))
    }));
  };

  handleCardButtonPress = player => {
    if (!player.targetCity.name) {
      this.selectRandomTargetCity(player);
    } else {
      this.setTargetCityHidden(player, !player.targetCity.hidden);
    }
  };

  render() {
    const { players } = this.state;
    return (
      <ScrollView>
        <Header
          placement="left"
          statusBarProps={{ translucent: true, backgroundColor: "transparent" }}
          leftComponent={{
            icon: "rotate-ccw",
            type: "feather",
            color: "white",
            onPress: this.handleResetButtonPress,
            underlayColor: "transparent"
          }}
          centerComponent={{
            text: "Elfenland Zielstadt",
            style: { color: "white" }
          }}
          rightComponent={
            <ButtonGroup
              onPress={this.handleNumberOfPlayersPress}
              selectedIndex={players.length - this.minPlayers}
              buttons={Array.from(
                { length: this.colors.length - this.minPlayers + 1 },
                (v, i) => i + this.minPlayers
              )}
              containerStyle={{ width: 120 }}
            />
          }
        />
        {players.map((player, index) => {
          return (
            <PricingCard
              color={player.color}
              title={`Spieler ${index + 1}`}
              key={player.color}
              price={player.targetCity.hidden ? "???" : player.targetCity.name}
              button={{
                title: player.targetCity.name
                  ? player.targetCity.hidden
                    ? "ANZEIGEN"
                    : "VERSTECKEN"
                  : "ZIELSTADT AUSWÄHLEN"
              }}
              onButtonPress={() => this.handleCardButtonPress(player)}
            />
          );
        })}
      </ScrollView>
    );
  }
}
