# Elfenland Target City

A companion app written with React Native for the board game Elfenland. You can randomly select a target city for every player, including cities that are not on the edge of the map as with the provided cards.

## Screenshot

<img src="/screenshot.png"  width="400" >

# Install

```
npm Install
```

# Build

```
node_modules/expo-cli/bin/expo.js build:android .
```

